import SomeModule from 'SomeModule.react.js'

document.addEventListener('DOMContentLoaded', () => {
  const rootOnPage = document.getElementById('root')
  ReactDOM.render(<SomeModule />, rootOnPage)
})
