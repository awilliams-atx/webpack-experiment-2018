const path = require('path')
const webpack = require('webpack')
const webpackAlias = require('./config/webpack.alias')
const webpackDefinePlugin = require('./config/webpack.define')
const webpackProvidePlugin = require('./config/webpack.provide.js')

module.exports = {
  context: __dirname,
  entry: [
    path.join(__dirname, 'index.js'),
    'webpack-hot-middleware/client',
  ],
  mode: 'development',
  output: {
    filename: 'bundle.js',
    path: __dirname,
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-react'],
          },
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    webpackDefinePlugin(),
    webpackProvidePlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    alias: webpackAlias,
  },
}
