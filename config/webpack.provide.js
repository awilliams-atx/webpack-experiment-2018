const webpack = require('webpack')

function webpackProvidePlugin() {
  return new webpack.ProvidePlugin({
    React: 'react',
    ReactDOM: 'react-dom',
  })
}
module.exports = webpackProvidePlugin
