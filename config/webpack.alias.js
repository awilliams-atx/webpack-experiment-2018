const path = require('path')
const _resolve = pathStr => {
  const pathSegments = pathStr.split('/')
  return path.resolve.apply(path, [process.cwd()].concat(pathSegments))
}

const webpackAlias = {
  'SomeModule.react.js': _resolve('src/some-module/SomeModule.react.js'),
  'SubModule.react.js': _resolve('src/sub-module/SubModule.react.js'),
}

module.exports = webpackAlias

