const webpack = require('webpack')

function webpackDefinePlugin() {
  return new webpack.DefinePlugin({
    'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    },
  })
}

module.exports = webpackDefinePlugin
