const express = require('express')
const fs = require('fs')
const path = require('path')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpackConfig = require('./webpack.config.js')

const app = express()

const compiler = webpack(webpackConfig)
const webpackDevMiddlewareConfig = {
  publicPath: webpackConfig.output.publicPath,
}
app.use(webpackDevMiddleware(compiler, webpackDevMiddlewareConfig))
app.use(webpackHotMiddleware(compiler))

const indexPath = path.resolve(__dirname, 'index.html')
const handle = (req, res) => fs.createReadStream(indexPath).pipe(res)
app.get('/', handle)

const PORT = 3000
const report = () => console.log(`Listening on ${PORT}`)
app.listen(PORT, () => report)
