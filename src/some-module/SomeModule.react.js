import SubModule from 'SubModule.react.js'
import './some-module.css'

import { hot } from 'react-hot-loader'

const SomeModule = () => <div>testing loadingg, main js module reporting<SubModule /></div>

// This goes at the root of the app
export default hot(module)(SomeModule)
